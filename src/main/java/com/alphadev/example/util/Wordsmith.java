package com.alphadev.example.util;

public class Wordsmith {

    public static final String EMPTY_STRING = "";
    public static final String BLANK_SPACE = " ";

    public String getReversedString(String inputText) {
        String[] words = inputText.split(BLANK_SPACE);
        String reversedString = EMPTY_STRING;
        for (String word : words) {
            String reverseWord = EMPTY_STRING;
            for (int j = word.length() - 1; j >= 0; j--) {
                reverseWord = reverseWord + word.charAt(j);
            }
            reversedString = reversedString + BLANK_SPACE + reverseWord;
        }
        return reversedString.trim();
    }
}

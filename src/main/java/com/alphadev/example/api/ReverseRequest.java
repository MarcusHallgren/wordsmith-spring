package com.alphadev.example.api;

import javax.validation.constraints.NotNull;

public class ReverseRequest {

    @NotNull
    private String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}

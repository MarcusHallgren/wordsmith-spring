package com.alphadev.example.service;

import com.alphadev.example.api.ReverseRequest;
import com.alphadev.example.persistance.entity.ReversedTextEntity;
import com.alphadev.example.persistance.repository.ReversedTextRepository;
import com.alphadev.example.util.Wordsmith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ReverseTextService {

    @Autowired
    ReversedTextRepository reversedTextRepository;

    public ReversedTextEntity handleReverseRequest(ReverseRequest reverseRequest) {
        String text = reverseRequest.getText();
        String reversedText = new Wordsmith().getReversedString(text);
        return saveResultToDb(text, reversedText);
    }

    public List<ReversedTextEntity> getAllReversed() {
        return reversedTextRepository.findAll();
    }

    public ReversedTextEntity saveResultToDb(String text, String reversedText) {
        ReversedTextEntity reversedTextEntity = new ReversedTextEntity();
        reversedTextEntity.setInitialText(text);
        reversedTextEntity.setReversedText(reversedText);
        return reversedTextRepository.save(reversedTextEntity);
    }

}

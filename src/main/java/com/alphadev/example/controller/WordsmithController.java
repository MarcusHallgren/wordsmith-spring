package com.alphadev.example.controller;

import com.alphadev.example.api.ReverseRequest;
import com.alphadev.example.persistance.entity.ReversedTextEntity;
import com.alphadev.example.service.ReverseTextService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController()
public class WordsmithController {

    @Autowired
    ReverseTextService reverseTextService;

    @PostMapping(value = "/reverse",
            produces = {MediaType.APPLICATION_JSON_VALUE},
            consumes = {MediaType.APPLICATION_JSON_VALUE})
    public ReversedTextEntity reverseText(@RequestBody ReverseRequest reverseRequest) {
        return reverseTextService.handleReverseRequest(reverseRequest);
    }

    @GetMapping(value = "reverse/list",
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public List<ReversedTextEntity> getReversed() {
        return reverseTextService.getAllReversed();
    }

}

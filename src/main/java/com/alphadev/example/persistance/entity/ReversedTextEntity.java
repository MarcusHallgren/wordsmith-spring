package com.alphadev.example.persistance.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import java.io.Serializable;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
public class ReversedTextEntity implements Serializable {

    private static final long serialVersionUID = -3847298471090274834L;

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private long id;

    @Column(nullable = false)
    private String initialText;

    @Column(nullable = false)
    private String reversedText;

    public long getId() {
        return id;
    }

    public String getInitialText() {
        return initialText;
    }

    public void setInitialText(String initialText) {
        this.initialText = initialText;
    }

    public String getReversedText() {
        return reversedText;
    }

    public void setReversedText(String reversedText) {
        this.reversedText = reversedText;
    }


}

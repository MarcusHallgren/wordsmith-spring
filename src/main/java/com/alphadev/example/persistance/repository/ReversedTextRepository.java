package com.alphadev.example.persistance.repository;

import com.alphadev.example.persistance.entity.ReversedTextEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ReversedTextRepository extends CrudRepository<ReversedTextEntity, Long> {

    List<ReversedTextEntity> findAll();

}

package com.alphadev.example;

import com.alphadev.example.utils.ResourceReader;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class WordsmithControllerTest {

    @Autowired
    private MockMvc mvc;

    private ResourceReader resourceReader;

    @Before
    public void setup() {
        resourceReader = new ResourceReader();
    }

    @Test
    public void shouldReturnContentTypeNotSupported() throws Exception {
        mvc.perform(MockMvcRequestBuilders.post("/reverse")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is(415)).andReturn();
    }

    @Test
    public void shouldReturnReversedString() throws Exception {
        String json = resourceReader.readFileResource("reverse-request.json");
        MvcResult result = mvc.perform(MockMvcRequestBuilders.post("/reverse")
                .contentType("application/json")
                .content(json)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.reversedText").value("esrever siht gnirts"))
                .andReturn();
    }
}
